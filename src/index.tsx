import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Routes } from './routes'
import { ApolloProvider } from 'react-apollo'
import { client } from './apollo'

 import registerServiceWorker from './registerServiceWorker';

import './index.css';


ReactDOM.render(
  <ApolloProvider client={client}>
    <Routes />
  </ApolloProvider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();