import * as React from 'react';
import MutationLoginForm from './MutationLoginForm'
import Form from './Form'
import { RouteComponentProps } from 'react-router-dom'
import { History } from 'history'
// import { ChildMutateProps } from 'react-apollo'
// import { loginMutationVariables, loginMutation } from './operation-result-types';

interface LoginProps extends RouteComponentProps<any> {
  history : History
}

class Login extends React.Component<
  // ChildMutateProps<
  //   LoginProps,loginMutation,loginMutationVariables
  // >,
  LoginProps,
  any> {
  
  public render() {
    return (
      <MutationLoginForm>
        {(login,loading) => <Form history={this.props.history} loading={loading} mutate={login}/>}
      </MutationLoginForm>
    )
  }
}




export default Login