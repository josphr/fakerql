import * as React from 'react'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'


interface QuerySingleTodoProps {
  children : any
}

const QuerySingleTodo : React.SFC<QuerySingleTodoProps> = ({
  children
}) => {
    return (
     <Query query={QuerySingleTodoById}>
      {(data) => {
        if(!data || data.loading) {
          return null
        }
        return children(data)
      }}
     </Query>
    )
}


const QuerySingleTodoById = gql`
  query getTodo($id:String!) {
    Todo(id:$id) {
      id 
      title
      completed
    }
  }
`

export default QuerySingleTodo