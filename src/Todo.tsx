import * as React from 'react'
import QuerySingleTodo from './QuerySingleTodo'

const Todo : React.SFC = () => {
    return (
        <QuerySingleTodo>
            {() => <h2>A Todo</h2>}
        </QuerySingleTodo>
    )
}

export default Todo