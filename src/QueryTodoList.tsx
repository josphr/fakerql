import * as React from 'react'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'


interface QueryTodoListProps {
  children : any
}

const QueryTodoList : React.SFC<QueryTodoListProps> = ({
  children
}) => {
    return (
     <Query query={QueryTodo}>
      {(data) => {
        if(!data || data.loading) {
          return null
        }
        return children(data)
      }}
     </Query>
    )
}


const QueryTodo = gql`
  query getTodos {
    allTodos(count:5) {
      id 
      title
      completed
    }
  }
`

export default QueryTodoList