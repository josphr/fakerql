import * as React from 'react';
import { Link } from 'react-router-dom'
import QueryTodoList from './QueryTodoList'

interface TodoList {
  todos : Todo[],
  map : any
}

interface Todo {
  id : string,
  completed : boolean,
  title : string
}

class User extends React.Component<any,any> {

  public mapToDoList = (todos:TodoList) : JSX.Element[] => {
    return todos.map((item:Todo) => (
        <li key={item.id}>
         <h5><Link to={`/todo/${item.id}`}>{item.title}</Link></h5>
        </li>
      )
    )
  }

  public render() : JSX.Element {
    return (
      <QueryTodoList>
        {({data} : any) => {
            if(data.loading || !data) { return <h2>Loading ... </h2> }
            return (
              <div>
                <section>
                  <h3>Todo List</h3>
                    <ul>
                    {this.mapToDoList(data.allTodos)}
                    </ul>
                </section>
              </div>)
          } 
        }
      </QueryTodoList>
    );
  }
}

export default User;
