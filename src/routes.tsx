import * as React from 'react'
import { BrowserRouter as Router , Route , Switch } from 'react-router-dom'
import Login from './Login'
import User from './User'
import Todo from './Todo'


export const Routes = () => {
    return (
        <Router>
            <Switch>
                <Route exact={true} path='/' component={Login} />
                <Route path='/user' component={User} />
                <Route path='/todo/:id' component={Todo} />
            </Switch>
        </Router>
    )
}