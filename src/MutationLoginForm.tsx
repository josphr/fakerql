import * as React from 'react';
import gql from 'graphql-tag'
import { Mutation  } from 'react-apollo'
import { loginMutation } from './operation-result-types'

export interface Props {
  children : (login : () => any ,loading:boolean) => JSX.Element,
}

const MutationLoginForm : React.SFC<Props> = ({
  children
}) => {  
    return (
        <Mutation mutation={loginMutation}>
          {(login,{loading}) => {
            if(loading) {
              return (<h4>loading ...</h4>)
            }
            return children(login,loading)
          }}
        </Mutation>
    );
}


const loginMutation = gql`
  mutation loginMutation($email:String!,$password:String!) {
    login(email:$email,password:$password) {
      token
    }
  }
`


export default MutationLoginForm