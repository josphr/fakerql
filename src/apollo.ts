import ApolloClient , { InMemoryCache } from 'apollo-boost'


export const client = new ApolloClient({
    uri : "https://fakerql.com/graphql",
    // request : async operator => {
    //     const token = await localStorage.getItem('token')
    //     operator.setContext({
    //       headers : {
    //         authorization : token ? `Bearer ${token}` : null
    //       }
    //     })
    // },
    cache : new InMemoryCache()
})

