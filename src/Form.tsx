import * as React from 'react'
import { ChildMutateProps } from 'react-apollo'
import { loginMutationVariables, loginMutation } from './operation-result-types'

interface FormProps {
    mutate : any,
    loading : boolean,
    history : {
        push(url: string): void;
    };
}

interface FormState {
    email : string,
    password : string,
    token : ''
}


type Props = ChildMutateProps<FormProps,loginMutation,loginMutationVariables>

class Form extends React.Component<Props,FormState> {
    constructor(props:Props){
        super(props)
        this.state = {
            email : '',
            password : '',
            token : ''
        }
    }
    
    public onInputChange = (e:React.FormEvent<HTMLInputElement>) : void => {
        const { name , value } = e.currentTarget
        this.setState(prevState => {
          return {
            ...prevState,
            [name] : value
          }
        })
    }

    public onSubmit = async (e:React.FormEvent<HTMLFormElement>) : Promise<any> => {
        e.preventDefault()
        const { data } = await this.props.mutate({
            variables : {
                email : this.state.email,
                password : this.state.password
            }
        })
        await this.props.history.push('/user')

        await console.log(data.login.token)
    }

    public render() {
        return (
            <form onSubmit={this.onSubmit}>
                <input
                    onChange={this.onInputChange}
                    value={this.state.email}
                    name="email"
                    type="email"
                    placeholder='Email' 
                />
                <input 
                    onChange={this.onInputChange}
                    value={this.state.password}
                    name="password"
                    type="password" 
                    placeholder='Password'
                />
                <button>OK</button>
            </form>
        )
    }
}

export default Form